# Observant Records 2015 Theme

A custom theme for [Observant Records](https://observantrecords.com/).

## Dependencies

* GruntJS

## Implementations

### Bootstrap Overrides for WordPress

`TemplateTags::paging_nav()` checks whether the Bootstrap Overrides plugin is enabled. If so,
it uses `Setup::wp_page_menu_args()` to pass arguments to the overridden paging functions.
See the [plugin documentation](https://bitbucket.org/NemesisVex/bootstrap-overrides-for-wordpress)
for usage.

### Observant Records Artist Connector

The `archive-album`, `content-album`, `content-track` and `sidebar-album` templates query the Observant Records artist database for discography data.

See the [plugin documentation](https://bitbucket.org/observantrecords/observant-records-artist-connector-for-wordpress)
for usage.

## Child Themes

* [Eponymous 4 2015](https://bitbucket.org/observantrecords/eponymous-4-theme-2015-for-wordpress)
* [Empty Ensemble 2015](https://bitbucket.org/observantrecords/empty-ensemble-theme-2015-for-wordpress)
* [Penzias and Wilson 2015](https://bitbucket.org/observantrecords/penzias-and-wilson-theme-2015-for-wordpress)
* [Shinkyoku Advocacy 2015](https://bitbucket.org/observantrecords/shinkyoku-advocacy-theme-2015-for-wordpress)
